package com.tutortrack.api.request;

/*
 * Author : Kundan Mishra
 * Date   : 09-June-2016
 */
public class ValidateNumberRequest {
	
	private String contactNumber;
	private String otpCode;

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getOtpCode() {
		return otpCode;
	}

	public void setOtpCode(String otpCode) {
		this.otpCode = otpCode;
	}

	public ValidateNumberRequest(String contactNumber, String otpCode) {
		this.contactNumber = contactNumber;
		this.otpCode = otpCode;
	}

	public ValidateNumberRequest() {
	}

	@Override
	public String toString() {
		return "ValidateNumberRequest [contactNumber=" + contactNumber + ", otpCode=" + otpCode + "]";
	}

}
