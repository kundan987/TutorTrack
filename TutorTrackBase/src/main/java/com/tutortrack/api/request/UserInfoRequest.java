package com.tutortrack.api.request;

public class UserInfoRequest {
	
	private String mobileOrEmail;
	private String password;
	
	public String getMobileOrEmail() {
		return mobileOrEmail;
	}
	public void setMobileOrEmail(String mobileOrEmail) {
		this.mobileOrEmail = mobileOrEmail;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String toString() {
		return "UserInfoRequest [mobileOrEmail=" + mobileOrEmail + ", password=" + password + "]";
	}

}
