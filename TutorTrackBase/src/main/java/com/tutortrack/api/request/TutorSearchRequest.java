package com.tutortrack.api.request;

import com.tutortrack.api.constants.SortBy;
import com.tutortrack.api.constants.TutorDetailFetchType;

/*
 * Author : Kundan Mishra
 * Date   : 14-June-2016
 */
public class TutorSearchRequest {
	private String studentMobile;
	private String studentId;
	private String studentEmail;
	private SortBy sortBy;
	private String searchParam;
	private Integer pageNumber;
	private Integer pageSize;
	private TutorDetailFetchType fetchType;

	public TutorSearchRequest() {
	}

	public TutorSearchRequest(String studentEmail,SortBy sortBy, String searchParam, Integer pageNumber, Integer pageSize,
			TutorDetailFetchType fetchType) {
		super();
		this.studentEmail = studentEmail;
		this.sortBy = sortBy;
		this.searchParam = searchParam;
		this.pageNumber = pageNumber;
		this.pageSize = pageSize;
		this.fetchType = fetchType;
	}

	public String getStudentMobile() {
		return studentMobile;
	}

	public void setStudentMobile(String studentMobile) {
		this.studentMobile = studentMobile;
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public String getStudentEmail() {
		return studentEmail;
	}

	public void setStudentEmail(String studentEmail) {
		this.studentEmail = studentEmail;
	}
	
	public SortBy getSortBy() {
		return sortBy;
	}

	public void setSortBy(SortBy sortBy) {
		this.sortBy = sortBy;
	}

	public String getSearchParam() {
		return searchParam;
	}

	public void setSearchParam(String searchParam) {
		this.searchParam = searchParam;
	}

	public Integer getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public TutorDetailFetchType getFetchType() {
		return fetchType;
	}

	public void setFetchType(TutorDetailFetchType fetchType) {
		this.fetchType = fetchType;
	}
}
