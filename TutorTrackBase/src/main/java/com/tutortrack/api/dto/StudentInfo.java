package com.tutortrack.api.dto;

import java.io.Serializable;
import java.util.Date;

/*
 * Author : Kundan Mishra
 * Date   : 13-June-2016
 */
public class StudentInfo implements Serializable{

	private static final long serialVersionUID = 2191646364366941417L;
	
	private String studentId;
	private String name;
	private String email;
	private String mobile;
	private String gender;
	private Date dob;
	private String password;
	private String address;
	private String city;
	private String state;
	private String pincode;
    private String location;
    private String imageUrl;
    private String board;
    private String classes;
    private String description;
	public String getStudentId() {
		return studentId;
	}
	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getBoard() {
		return board;
	}
	public void setBoard(String board) {
		this.board = board;
	}
	public String getClasses() {
		return classes;
	}
	public void setClasses(String classes) {
		this.classes = classes;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Override
	public String toString() {
		return "StudentInfo [studentId=" + studentId + ", name=" + name + ", email=" + email + ", mobile=" + mobile
				+ ", gender=" + gender + ", dob=" + dob + ", password=" + password + ", address=" + address + ", city="
				+ city + ", state=" + state + ", pincode=" + pincode + ", location=" + location + ", imageUrl="
				+ imageUrl + ", board=" + board + ", classes=" + classes + ", description=" + description + "]";
	}
}
