package com.tutortrack.api.dto;

import java.io.Serializable;
import java.util.Date;

/*
 * Author : Kundan Mishra
 * Date   : 13-June-2016
 */
public class TutorInfo implements Serializable{

	private static final long serialVersionUID = -2371955275888712608L;
	
	private String tutorId;
	private String name;
	private String email;
	private String mobile;
	private String gender;
	private Date dob;
	private String password;
	private String address;
	private String city;
	private String state;
	private String pincode;
    private String location;
    private String imageUrl;
    private String qualification;
    private String experience;
    private String description;
	public String getTutorId() {
		return tutorId;
	}
	public void setTutorId(String tutorId) {
		this.tutorId = tutorId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getQualification() {
		return qualification;
	}
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}
	public String getExperience() {
		return experience;
	}
	public void setExperience(String experience) {
		this.experience = experience;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Override
	public String toString() {
		return "TutorInfo [tutorId=" + tutorId + ", name=" + name + ", email=" + email + ", mobile=" + mobile
				+ ", gender=" + gender + ", dob=" + dob + ", password=" + password + ", address=" + address + ", city="
				+ city + ", state=" + state + ", pincode=" + pincode + ", location=" + location + ", imageUrl="
				+ imageUrl + ", qualification=" + qualification + ", experience=" + experience + ", description="
				+ description + "]";
	}
}
