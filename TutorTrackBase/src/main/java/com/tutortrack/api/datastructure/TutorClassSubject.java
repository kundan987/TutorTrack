package com.tutortrack.api.datastructure;

public class TutorClassSubject {

	private String tutorId;
	private String classes;
	private String subject;
	private Integer fee;
	
	public String getTutorId() {
		return tutorId;
	}
	public void setTutorId(String tutorId) {
		this.tutorId = tutorId;
	}
	public String getClasses() {
		return classes;
	}
	public void setClasses(String classes) {
		this.classes = classes;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public Integer getFee() {
		return fee;
	}
	public void setFee(Integer fee) {
		this.fee = fee;
	}
	
	@Override
	public String toString() {
		return "TutorClassSubject [tutorId=" + tutorId + ", classes=" + classes + ", subject=" + subject + ", fee="
				+ fee + "]";
	}
}
