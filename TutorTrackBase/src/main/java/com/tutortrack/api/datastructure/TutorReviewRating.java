package com.tutortrack.api.datastructure;

public class TutorReviewRating {

	private String tutorId;
	private float rating;
	private String review;
	private String studentId;
	
	public String getTutorId() {
		return tutorId;
	}
	public void setTutorId(String tutorId) {
		this.tutorId = tutorId;
	}
	public float getRating() {
		return rating;
	}
	public void setRating(float rating) {
		this.rating = rating;
	}
	public String getReview() {
		return review;
	}
	public void setReview(String review) {
		this.review = review;
	}
	public String getStudentId() {
		return studentId;
	}
	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}
	
	@Override
	public String toString() {
		return "TutorReviewRating [tutorId=" + tutorId + ", rating=" + rating + ", review=" + review + ", studentId="
				+ studentId + "]";
	}
}
