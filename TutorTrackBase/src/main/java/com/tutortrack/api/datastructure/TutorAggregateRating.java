package com.tutortrack.api.datastructure;

public class TutorAggregateRating {
	
	private float avgRating;
	private Integer numReviews;
	private String tutorId;
	
	public float getAvgRating() {
		return avgRating;
	}
	public void setAvgRating(float avgRating) {
		this.avgRating = avgRating;
	}
	public Integer getNumReviews() {
		return numReviews;
	}
	public void setNumReviews(Integer numReviews) {
		this.numReviews = numReviews;
	}
	public String getTutorId() {
		return tutorId;
	}
	public void setTutorId(String tutorId) {
		this.tutorId = tutorId;
	}
	
	@Override
	public String toString() {
		return "TutorAggregateRating [avgRating=" + avgRating + ", numReviews=" + numReviews + ", tutorId=" + tutorId
				+ "]";
	}
}
