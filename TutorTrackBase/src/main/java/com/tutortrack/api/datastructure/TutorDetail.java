package com.tutortrack.api.datastructure;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/*
 * Author : Kundan Mishra
 * Date   : 14-June-2016
 */
public class TutorDetail implements Serializable{

	private static final long serialVersionUID = 1771423401536659262L;
	
	private String tutorId;
	private String name;
	private String email;
	private String mobile;
	private String gender;
	private Date dob;
	private String address;
	private String city;
	private String state;
	private String pincode;
	private String location;
    private String imageUrl;
    private String qualification;
    public String experience;
    private String description;
	public float rating;
	private Integer numreviews;
	private String distance;
	public Integer fees;
	private List<String> teaches;
	private List<TutorReviewRating> tutorReviewRating;
	private List<TutorClassSubject> tutorClassSubject;
	public String getTutorId() {
		return tutorId;
	}
	public void setTutorId(String tutorId) {
		this.tutorId = tutorId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getQualification() {
		return qualification;
	}
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}
	public String getExperience() {
		return experience;
	}
	public void setExperience(String experience) {
		this.experience = experience;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public float getRating() {
		return rating;
	}
	public void setRating(float rating) {
		this.rating = rating;
	}
	public Integer getNumreviews() {
		return numreviews;
	}
	public void setNumreviews(Integer numreviews) {
		this.numreviews = numreviews;
	}
	public String getDistance() {
		return distance;
	}
	public void setDistance(String distance) {
		this.distance = distance;
	}
	public Integer getFees() {
		return fees;
	}
	public void setFees(Integer fees) {
		this.fees = fees;
	}
	public List<String> getTeaches() {
		return teaches;
	}
	public void setTeaches(List<String> teaches) {
		this.teaches = teaches;
	}
	public List<TutorReviewRating> getTutorReviewRating() {
		return tutorReviewRating;
	}
	public void setTutorReviewRating(List<TutorReviewRating> tutorReviewRating) {
		this.tutorReviewRating = tutorReviewRating;
	}
	public List<TutorClassSubject> getTutorClassSubject() {
		return tutorClassSubject;
	}
	public void setTutorClassSubject(List<TutorClassSubject> tutorClassSubject) {
		this.tutorClassSubject = tutorClassSubject;
	}
	@Override
	public String toString() {
		return "TutorDetail [tutorId=" + tutorId + ", name=" + name + ", email=" + email + ", mobile=" + mobile
				+ ", gender=" + gender + ", dob=" + dob + ", address=" + address + ", city=" + city + ", state=" + state
				+ ", pincode=" + pincode + ", location=" + location + ", imageUrl=" + imageUrl + ", qualification="
				+ qualification + ", experience=" + experience + ", description=" + description + ", rating=" + rating
				+ ", numreviews=" + numreviews + ", distance=" + distance + ", fees=" + fees + ", teaches=" + teaches
				+ ", tutorReviewRating=" + tutorReviewRating + ", tutorClassSubject=" + tutorClassSubject + "]";
	}
}
