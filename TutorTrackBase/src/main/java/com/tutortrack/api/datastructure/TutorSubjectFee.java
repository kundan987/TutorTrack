package com.tutortrack.api.datastructure;

public class TutorSubjectFee {
	
	private String subject;
	private String fee;
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getFee() {
		return fee;
	}
	public void setFee(String fee) {
		this.fee = fee;
	}
	@Override
	public String toString() {
		return "TutorSubjectFee [subject=" + subject + ", fee=" + fee + "]";
	}
}
