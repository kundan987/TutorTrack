package com.tutortrack.api.response;

import com.tutortrack.api.dto.StudentInfo;
import com.tutortrack.api.dto.TutorInfo;

public class UserInfoResponse extends TutorTrackResponse{
	
	private static final long serialVersionUID = 2914958300890890209L;
	
	private StudentInfo studentInfo;
	private TutorInfo tutorInfo;
	
	public StudentInfo getStudentInfo() {
		return studentInfo;
	}
	public void setStudentInfo(StudentInfo studentInfo) {
		this.studentInfo = studentInfo;
	}
	public TutorInfo getTutorInfo() {
		return tutorInfo;
	}
	public void setTutorInfo(TutorInfo tutorInfo) {
		this.tutorInfo = tutorInfo;
	}
	@Override
	public String toString() {
		return "UserInfoResponse [studentInfo=" + studentInfo + ", tutorInfo=" + tutorInfo + "]";
	}

}
