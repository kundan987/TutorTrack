package com.tutortrack.api.response;

import com.tutortrack.api.dto.TutorInfo;

public class TutorInfoResponse extends TutorTrackResponse{

	private static final long serialVersionUID = 4639631934417067096L;
	
	private TutorInfo tutorInfo;

	public TutorInfo getTutorInfo() {
		return tutorInfo;
	}

	public void setTutorInfo(TutorInfo tutorInfo) {
		this.tutorInfo = tutorInfo;
	}

	@Override
	public String toString() {
		return "TutorInfoResponse [tutorInfo=" + tutorInfo + "]";
	}
}
