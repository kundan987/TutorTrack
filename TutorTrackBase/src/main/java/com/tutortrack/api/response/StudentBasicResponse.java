package com.tutortrack.api.response;

public class StudentBasicResponse extends TutorTrackResponse{

	private static final long serialVersionUID = 5628023250249895178L;
	
	private String studentId;
	private String mobile;
	private String email;
	private String name;
	private Integer otpCode;
	
	public String getStudentId() {
		return studentId;
	}
	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getOtpCode() {
		return otpCode;
	}
	public void setOtpCode(Integer otpCode) {
		this.otpCode = otpCode;
	}
	@Override
	public String toString() {
		return "StudentBasicResponse [studentId=" + studentId + ", mobile=" + mobile + ", email=" + email + ", name="
				+ name + ", otpCode=" + otpCode + "]";
	}
}
