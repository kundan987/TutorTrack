package com.tutortrack.api.response;

import com.tutortrack.api.dto.StudentInfo;

public class StudentInfoResponse extends TutorTrackResponse{

	private static final long serialVersionUID = -4579773188601464667L;
	
	private StudentInfo studentInfo;

	public StudentInfo getStudentInfo() {
		return studentInfo;
	}

	public void setStudentInfo(StudentInfo studentInfo) {
		this.studentInfo = studentInfo;
	}

	@Override
	public String toString() {
		return "StudentInfoResponse [studentInfo=" + studentInfo + "]";
	}

}
