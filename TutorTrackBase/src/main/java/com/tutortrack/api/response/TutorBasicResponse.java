package com.tutortrack.api.response;

public class TutorBasicResponse extends TutorTrackResponse{

	private static final long serialVersionUID = -8046303237520566190L;
	
	private String tutorId;
	private String mobile;
	private String email;
	private String name;
	private Integer otpCode;
	
	public String getTutorId() {
		return tutorId;
	}
	public void setTutorId(String tutorId) {
		this.tutorId = tutorId;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getOtpCode() {
		return otpCode;
	}
	public void setOtpCode(Integer otpCode) {
		this.otpCode = otpCode;
	}
	@Override
	public String toString() {
		return "TutorBasicResponse [tutorId=" + tutorId + ", mobile=" + mobile + ", email=" + email + ", name=" + name
				+ ", otpCode=" + otpCode + "]";
	}
}
