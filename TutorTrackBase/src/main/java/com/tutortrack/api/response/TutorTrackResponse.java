package com.tutortrack.api.response;

import java.io.Serializable;

/*
 * Author : Kundan Mishra
 * Date   : 09-June-2016
 */
public class TutorTrackResponse implements Serializable{
	private static final long serialVersionUID = 1L;

	private String status;
	private String errorCode;
 	private String message;
	
	public TutorTrackResponse(){
	this.status = "FAILURE";
	this.errorCode="";
	this.message="";
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getMessage() {
 		return message;
 	}
 
 	public void setMessage(String message) {
 		this.message = message;
 	}

}