package com.tutortrack.api.response;

/*
 * Author : Kundan Mishra
 * Date   : 09-June-2016
 */
public class ValidateNumberResponse extends TutorTrackResponse{

	private static final long serialVersionUID = 7456706308300312252L;
	private boolean validated;

	public ValidateNumberResponse(boolean validated) {
		this.validated = validated;
	}

	public ValidateNumberResponse() {
	}

	public boolean isValidated() {
		return validated;
	}

	public void setValidated(boolean validated) {
		this.validated = validated;
	}

	@Override
	public String toString() {
		return "ValidateNumberResponse [validated=" + validated + "]";
	}
}
