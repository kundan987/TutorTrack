package com.tutortrack.api.response;

import java.util.ArrayList;
import java.util.List;

import com.tutortrack.api.datastructure.TutorDetail;

public class TutorSearchResponse extends TutorTrackResponse{

	private static final long serialVersionUID = 1778251275755314265L;
	
	private List<TutorDetail> tutors = new ArrayList<TutorDetail>();

	public List<TutorDetail> getTutors() {
		return tutors;
	}

	public void setTutors(List<TutorDetail> tutors) {
		this.tutors = tutors;
	}

	@Override
	public String toString() {
		return "TutorSearchResponse [tutors=" + tutors + "]";
	}
}
