package com.tutortrack.api.dao;

/*
 * Author : Kundan Mishra
 * Date   : 13-June-2016
 */
public class QueryConstants {
	public static final String INSERT_TUTOR_INFO = 
			(new StringBuilder("INSERT into tutor_info (tutor_id, name, email, mobile, gender, dob, password, ")
			.append("address, city, state, pincode, location, image_url, qualification, experience, description, created, updated) ")
			.append(" VALUES ( ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")).toString();
	
	public static final String UPDATE_TUTOR_INFO = 
			(new StringBuilder("UPDATE tutor_info SET gender=?, dob=?, password=?, address=?, city=?, state=?, pincode=?, ")
			.append("location=?, image_url=?, qualification=?, experience=?, description=?, updated=? ")
			.append(" WHERE mobile=?")).toString();
	
	public static final String INSERT_STUDENT_INFO = 
			(new StringBuilder("INSERT into student_info (student_id, name, email, mobile, gender, dob, password, ")
			.append("address, city, state, pincode, location, image_url, board, class, description, created, updated) ")
			.append(" VALUES ( ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")).toString();
	
	public static final String UPDATE_STUDENT_INFO = 
			(new StringBuilder("UPDATE student_info SET gender = ?, dob=?, password=?, ")
			.append("address=?, city=?, state=?, pincode=?, location=?, image_url=?, board=?, class=?, description=?, updated=? ")
			.append(" WHERE mobile =?")).toString();
	
	public static final String INSERT_TUTOR_CLASS_SUBJECT_MAPPING = 
			(new StringBuilder("INSERT into tutor_class_subject_mapping (class, subject, fee, tutor_id, created, updated) ")
			.append(" VALUES ( ?,?,?,?,?,?)")).toString();
	
	public static final String INSERT_TUTOR_REVIEW_RATING_MAPPING = 
			(new StringBuilder("INSERT into tutor_review_rating_mapping (rating, review, tutor_id, student_id, created, updated) ")
			.append(" VALUES ( ?,?,?,?,?,?)")).toString();
	
	public static final String INSERT_TUTOR_AGGREGATE_RATING = 
			(new StringBuilder("INSERT into tutor_aggregate_rating (avg_rating, num_reviews, tutor_id, created, updated) ")
			.append(" VALUES ( ?,?,?,?,?)")).toString();
	
	public static final String SELECT_TUTORS_DETAIL_BASIC =
			(new StringBuilder("SELECT tutor_id AS tutorId, name AS name, location AS location, experience AS experience,")
			.append("image_url AS imageUrl FROM tutor_info")).toString();
	
	public static final String SELECT_TUTORS_SORTED_DETAIL_BASIC =
			(new StringBuilder("SELECT tutor_id AS tutorId, name AS name, location AS location, experience AS experience,")
			.append("image_url AS imageUrl FROM tutor_info ORDER BY ? ?")).toString();
	
	public static final String SELECT_TUTORS_DETAIL_FULL =
			(new StringBuilder("SELECT tutor_id AS tutorId, name, email, mobile, gender, dob, location, address, city, state, pincode,")
			.append("location, qualification, experience, description, image_url AS imageUrl FROM tutor_info")).toString();
	
	public static final String SELECT_TUTOR_DETAIL =
			(new StringBuilder("SELECT tutor_id AS tutorId, name, email, mobile, gender, dob, location, address, city, state, pincode,")
			.append("location, password, qualification, experience, description, image_url AS imageUrl FROM tutor_info WHERE tutor_id = ?")).toString();
	
	public static final String SELECT_STUDENT_DETAIL =
			(new StringBuilder("SELECT student_id AS studentId, name, email, mobile, gender, dob, location, address, city, state, pincode,")
			.append("location, password, class AS classes, board, description, image_url AS imageUrl FROM student_info WHERE student_id = ?")).toString();
	
	public static final String SELECT_STUDENT_DETAIL_WITH_EMAIL =
			(new StringBuilder("SELECT student_id AS studentId, class AS class, board AS board, location As location FROM student_info WHERE email = ?")).toString();
	
	public static final String SELECT_STUDENT_DETAIL_WITH_MOBILE =
			(new StringBuilder("SELECT student_id AS studentId, class AS class, board AS board, location As location FROM student_info WHERE mobile = ?")).toString();
	
	public static final String SELECT_TUTOR_ID_WITH_EMAIL =
			(new StringBuilder("SELECT tutor_id FROM tutor_info WHERE email = ?")).toString();
	
	public static final String SELECT_TUTOR_ID_WITH_MOBILE =
			(new StringBuilder("SELECT tutor_id FROM tutor_info WHERE mobile = ?")).toString();
	
	public static final String SELECT_STUDENT_ID_WITH_EMAIL =
			(new StringBuilder("SELECT student_id FROM student_info WHERE email = ?")).toString();
	
	public static final String SELECT_STUDENT_ID_WITH_MOBILE =
			(new StringBuilder("SELECT student_id FROM student_info WHERE mobile = ?")).toString();
	
	public static final String SELECT_TUTOR_AGGREGATE_RATING_BY_TUTOR_ID =
			(new StringBuilder("SELECT avg_rating AS avgRating, num_reviews AS numReviews FROM tutor_aggregate_rating WHERE tutor_id = ?")).toString();
	
	public static final String SELECT_TUTOR_SUBJECTS_BY_TUTOR_ID =
			(new StringBuilder("SELECT class AS classes, subject AS subject, fee AS fee FROM tutor_class_subject_mapping WHERE tutor_id = ?")).toString();
	
	public static final String SELECT_TUTOR_RATING_BY_TUTOR_ID =
			(new StringBuilder("SELECT rating AS rating, review AS review, student_id AS studentId FROM tutor_review_rating_mapping WHERE tutor_id = ?")).toString();
	
	public static final String SELECT_RATING_COUNT_BY_TUTOR_ID =
			(new StringBuilder("SELECT COUNT(tutor_id) FROM tutor_review_rating_mapping WHERE tutor_id = ?")).toString();
	
	public static final String UPDATE_TUTOR_AGGREGATE_RATING = 
			(new StringBuilder("UPDATE tutor_aggregate_rating SET num_reviews = num_reviews + ?,")
			.append("avg_rating = (((avg_rating * ?) + ?)/(?+1)), updated= ? WHERE tutor_id =?")).toString();
}
