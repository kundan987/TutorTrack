package com.tutortrack.api.dao;

import java.util.Date;
import java.util.List;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.tutortrack.api.datastructure.TutorAggregateRating;
import com.tutortrack.api.datastructure.TutorClassSubject;
import com.tutortrack.api.datastructure.TutorReviewRating;
import com.tutortrack.api.dto.StudentInfo;
import com.tutortrack.api.dto.TutorInfo;

/*
 * Author : Kundan Mishra
 * Date   : 13-June-2016
 */
@Repository("tutorTrackServiceDao")
public class TutorTrackServiceDaoImpl extends AbstractDao implements ITutorTrackServiceDao{

	@Override
	public int insertTutorInfo(TutorInfo tutorInfo) {
		Object[] inputs = prepareInsertTutorInfo(tutorInfo);
		return getJdbcTemplate().update(QueryConstants.INSERT_TUTOR_INFO, inputs);
	}

	private Object[] prepareInsertTutorInfo(TutorInfo tutorInfo) {
		Object[] inputs = new Object[] { tutorInfo.getTutorId(), tutorInfo.getName(), tutorInfo.getEmail(), 
				tutorInfo.getMobile(), tutorInfo.getGender(), tutorInfo.getDob(), tutorInfo.getPassword(),
				tutorInfo.getAddress(), tutorInfo.getCity(), tutorInfo.getState(), tutorInfo.getPincode(),
				tutorInfo.getLocation(), tutorInfo.getImageUrl(), tutorInfo.getQualification(), 
				tutorInfo.getExperience(), tutorInfo.getDescription(), new Date(), new Date()
		};
		return inputs;
	}
	
	@Override
	public int insertStudentInfo(StudentInfo studentInfo) {
		Object[] inputs = prepareInsertStudentInfo(studentInfo);
		return getJdbcTemplate().update(QueryConstants.INSERT_STUDENT_INFO, inputs);
	}

	private Object[] prepareInsertStudentInfo(StudentInfo studentInfo) {
		Object[] inputs = new Object[] { studentInfo.getStudentId(), studentInfo.getName(), studentInfo.getEmail(), 
				studentInfo.getMobile(), studentInfo.getGender(), studentInfo.getDob(), studentInfo.getPassword(),
				studentInfo.getAddress(), studentInfo.getCity(), studentInfo.getState(), studentInfo.getPincode(),
				studentInfo.getLocation(), studentInfo.getImageUrl(), studentInfo.getBoard(), studentInfo.getClasses(),
				studentInfo.getDescription(), new Date(), new Date()
		};
		return inputs;
	}
	
	@Override
	public int insertTutorClassSubjectMapping(TutorClassSubject tutorClassSubject) {
		Object[] inputs = new Object[] {tutorClassSubject.getClasses(), tutorClassSubject.getSubject(), 
				tutorClassSubject.getFee(), tutorClassSubject.getTutorId(), new Date(), new Date()};
		return getJdbcTemplate().update(QueryConstants.INSERT_TUTOR_CLASS_SUBJECT_MAPPING, inputs);
	}
	
	@Override
	public int insertTutorReviewRatingMapping(TutorReviewRating tutorReviewRating) {
		Object[] inputs = new Object[] {tutorReviewRating.getRating(), tutorReviewRating.getReview(), 
				tutorReviewRating.getTutorId(), tutorReviewRating.getStudentId(), new Date(), new Date()};
		return getJdbcTemplate().update(QueryConstants.INSERT_TUTOR_REVIEW_RATING_MAPPING, inputs);
	}
	
	@Override
	public int insertTutorAggregateRating(TutorAggregateRating tutorAggregateRating) {
		Object[] inputs = new Object[] {tutorAggregateRating.getAvgRating(), tutorAggregateRating.getNumReviews(), 
				tutorAggregateRating.getTutorId(), new Date(), new Date()};
		return getJdbcTemplate().update(QueryConstants.INSERT_TUTOR_AGGREGATE_RATING, inputs);
	}
	
	@Override
	public List<TutorInfo> getTutorsBasicInfo() {
		List<TutorInfo> tutorInfo = getJdbcTemplate().query(
				QueryConstants.SELECT_TUTORS_DETAIL_BASIC,
				new BeanPropertyRowMapper<TutorInfo>(TutorInfo.class));
		return tutorInfo;
	}
	
	@Override
	public List<TutorInfo> getTutorsSortedBasicInfo(String sortby, String order) {
		Object[] inputs = new Object[]{sortby,order};
		List<TutorInfo> tutorInfo = getJdbcTemplate().query(
				QueryConstants.SELECT_TUTORS_SORTED_DETAIL_BASIC,inputs,
				new BeanPropertyRowMapper<TutorInfo>(TutorInfo.class));
		return tutorInfo;
	}
	
	@Override
	public TutorInfo getTutorInfo(String tutorId) {
		Object[] inputs = new Object[] { tutorId };
		List<TutorInfo> tutorInfo = getJdbcTemplate().query(QueryConstants.SELECT_TUTOR_DETAIL, inputs,
				new BeanPropertyRowMapper<TutorInfo>(TutorInfo.class));
		return tutorInfo.get(0);
	}
	
	@Override
	public List<TutorInfo> getTutorsFullInfo() {
		List<TutorInfo> tutorInfo = getJdbcTemplate().query(
				QueryConstants.SELECT_TUTORS_DETAIL_FULL,
				new BeanPropertyRowMapper<TutorInfo>(TutorInfo.class));
		return tutorInfo;
	}
	
	@Override
	public TutorAggregateRating getTutorAggregateRating(String tutorId) {
		Object[] inputs = new Object[] { tutorId };
		List<TutorAggregateRating> tutorAggregateRating = getJdbcTemplate().query(QueryConstants.SELECT_TUTOR_AGGREGATE_RATING_BY_TUTOR_ID, inputs,
				new BeanPropertyRowMapper<TutorAggregateRating>(TutorAggregateRating.class));
		return tutorAggregateRating.get(0);
	}
	
	@Override
	public List<TutorClassSubject> getTutorSubjects(String tutorId) {
		Object[] inputs = new Object[] { tutorId };
		List<TutorClassSubject> tutorClassSubject = getJdbcTemplate().query(QueryConstants.SELECT_TUTOR_SUBJECTS_BY_TUTOR_ID, inputs,
				new BeanPropertyRowMapper<TutorClassSubject>(TutorClassSubject.class));
		return tutorClassSubject;
	}
	
	@Override
	public List<TutorReviewRating> getTutorRatingReviews(String tutorId) {
		Object[] inputs = new Object[] { tutorId };
		List<TutorReviewRating> tutorReviewRating = getJdbcTemplate().query(QueryConstants.SELECT_TUTOR_RATING_BY_TUTOR_ID, inputs,
				new BeanPropertyRowMapper<TutorReviewRating>(TutorReviewRating.class));
		return tutorReviewRating;
	}
	
	@Override
	public Integer getTutorRatingCount(String tutorId) {
		Object[] inputs = new Object[] { tutorId };
		return getJdbcTemplate().queryForObject(QueryConstants.SELECT_RATING_COUNT_BY_TUTOR_ID, inputs,
				Integer.class);
	}
	
	@Override
	public int updateTutorAggregateRating(Integer numReviews, Integer count, float rating, String tutorId) {
		Object[] inputs = new Object[] { numReviews, count, rating, count, new Date(), tutorId};
		return getJdbcTemplate().update(QueryConstants.UPDATE_TUTOR_AGGREGATE_RATING, inputs);
	}
	
	@Override
	public String getTutorIdFromMobile(String mobile) {
		Object[] inputs = new Object[] { mobile };
		try{
			return getJdbcTemplate().queryForObject(QueryConstants.SELECT_TUTOR_ID_WITH_MOBILE, inputs,
					String.class);
		}catch (EmptyResultDataAccessException e){
			return null;
		}
	}
	
	@Override
	public String getTutorIdFromEmail(String email) {
		Object[] inputs = new Object[] { email };
		try{
			return getJdbcTemplate().queryForObject(QueryConstants.SELECT_TUTOR_ID_WITH_EMAIL, inputs,
					String.class);
		}catch (EmptyResultDataAccessException e) {
			return null;
		}		
	}
	
	@Override
	public String getStudentIdFromMobile(String mobile) {
		Object[] inputs = new Object[] { mobile };
		try{
			return getJdbcTemplate().queryForObject(QueryConstants.SELECT_STUDENT_ID_WITH_MOBILE, inputs,
					String.class);
		}catch (EmptyResultDataAccessException e){
			return null;
		}
	}
	
	@Override
	public String getStudentIdFromEmail(String email) {
		Object[] inputs = new Object[] { email };
		try{
			return getJdbcTemplate().queryForObject(QueryConstants.SELECT_STUDENT_ID_WITH_EMAIL, inputs,
					String.class);
		}catch (EmptyResultDataAccessException e){
			return null;
		}
	}
	
	@Override
	public StudentInfo getStudentInfo(String studentId) {
		Object[] inputs = new Object[] { studentId };
		List<StudentInfo> studentInfo = getJdbcTemplate().query(QueryConstants.SELECT_STUDENT_DETAIL, inputs,
				new BeanPropertyRowMapper<StudentInfo>(StudentInfo.class));
		return studentInfo.get(0);
	}
	
	@Override
	public int updateTutorInfo(TutorInfo tutorInfo) {
		Object[] inputs = prepareUpdateTutorInfo(tutorInfo);
		return getJdbcTemplate().update(QueryConstants.UPDATE_TUTOR_INFO, inputs);
	}
	
	private Object[] prepareUpdateTutorInfo(TutorInfo tutorInfo) {
		Object[] inputs = new Object[] { tutorInfo.getGender(), tutorInfo.getDob(), tutorInfo.getPassword(),
				tutorInfo.getAddress(), tutorInfo.getCity(), tutorInfo.getState(), tutorInfo.getPincode(),
				tutorInfo.getLocation(), tutorInfo.getImageUrl(), tutorInfo.getQualification(), 
				tutorInfo.getExperience(), tutorInfo.getDescription(), new Date(), tutorInfo.getMobile()
		};
		return inputs;
	}
	
	@Override
	public int updateStudentInfo(StudentInfo studentInfo) {
		Object[] inputs = prepareUpdateStudentInfo(studentInfo);
		return getJdbcTemplate().update(QueryConstants.UPDATE_STUDENT_INFO, inputs);
	}
	
	private Object[] prepareUpdateStudentInfo(StudentInfo studentInfo) {
		Object[] inputs = new Object[] { studentInfo.getGender(), studentInfo.getDob(), studentInfo.getPassword(),
				studentInfo.getAddress(), studentInfo.getCity(), studentInfo.getState(), studentInfo.getPincode(),
				studentInfo.getLocation(), studentInfo.getImageUrl(), studentInfo.getBoard(), studentInfo.getClasses(),
				studentInfo.getDescription(), new Date(), studentInfo.getMobile()
		};
		return inputs;
	}
}
