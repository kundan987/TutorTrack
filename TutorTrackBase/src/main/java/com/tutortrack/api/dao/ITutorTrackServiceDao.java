package com.tutortrack.api.dao;

import java.util.List;

import com.tutortrack.api.datastructure.TutorAggregateRating;
import com.tutortrack.api.datastructure.TutorClassSubject;
import com.tutortrack.api.datastructure.TutorReviewRating;
import com.tutortrack.api.dto.StudentInfo;
import com.tutortrack.api.dto.TutorInfo;

/*
 * Author : Kundan Mishra
 * Date   : 13-June-2016
 */
public interface ITutorTrackServiceDao {
	
	int insertTutorInfo(TutorInfo tutorInfo);

	int insertStudentInfo(StudentInfo studentInfo);

	List<TutorInfo> getTutorsBasicInfo();

	TutorAggregateRating getTutorAggregateRating(String tutorId);

	List<TutorClassSubject> getTutorSubjects(String tutorId);

	int insertTutorClassSubjectMapping(TutorClassSubject tutorClassSubject);

	int insertTutorReviewRatingMapping(TutorReviewRating tutorReviewRating);

	int insertTutorAggregateRating(TutorAggregateRating tutorAggregateRating);

	Integer getTutorRatingCount(String tutorId);

	int updateTutorAggregateRating(Integer numReviews, Integer count, float rating, String tutorId);

	List<TutorReviewRating> getTutorRatingReviews(String tutorId);

	List<TutorInfo> getTutorsFullInfo();

	TutorInfo getTutorInfo(String tutorId);

	String getTutorIdFromMobile(String mobile);

	String getTutorIdFromEmail(String email);

	String getStudentIdFromMobile(String mobile);

	String getStudentIdFromEmail(String email);

	StudentInfo getStudentInfo(String studentId);

	int updateTutorInfo(TutorInfo tutorInfo);

	int updateStudentInfo(StudentInfo studentInfo);

	List<TutorInfo> getTutorsSortedBasicInfo(String sortby, String order);

}
