package com.tutortrack.api.constants;

/*
 * Author : Kundan Mishra
 * Date   : 14-June-2016
 */
public enum TutorDetailFetchType {
	FETCH_WITH_BASIC_DETAILS,
	FETCH_WITH_ALL_DETAILS;

}
