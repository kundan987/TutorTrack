package com.tutortrack.api.exception;

/*
 * Author : Kundan Mishra
 * Date   : 08-June-2016
 */
public  class TutorTrackException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private ErrorCode errorCode;


	public TutorTrackException(ErrorCode error){
		this.errorCode = error;
	}

	public TutorTrackException(ErrorCode error, String message){
		super(message);
		this.errorCode = error;
	}

	public TutorTrackException(ErrorCode error, String message, Throwable cause){
		super(message, cause);
		this.errorCode = error;
	}

	public ErrorCode getErrorCode() {
		return errorCode;
	}

}
