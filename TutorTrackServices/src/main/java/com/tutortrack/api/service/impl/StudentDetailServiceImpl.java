package com.tutortrack.api.service.impl;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tutortrack.api.dao.ITutorTrackServiceDao;
import com.tutortrack.api.dto.StudentInfo;
import com.tutortrack.api.exception.ErrorCode;
import com.tutortrack.api.exception.TutorTrackException;
import com.tutortrack.api.request.StudentInfoRequest;
import com.tutortrack.api.response.StudentInfoResponse;
import com.tutortrack.api.service.IStudentDetailService;

@Service("studentDetailService")
public class StudentDetailServiceImpl implements IStudentDetailService {

	@Autowired
	ITutorTrackServiceDao tutorTrackServiceDao;

	private static final Logger LOG = LoggerFactory.getLogger(StudentDetailServiceImpl.class);

	@Override
	public StudentInfoResponse getStudentInfo(StudentInfoRequest request) {
		StudentInfoResponse response = new StudentInfoResponse();
		String studentId = null;
		try {
			LOG.info(request.getMobileOrEmail());
			if (NumberUtils.isNumber(request.getMobileOrEmail())) {
				studentId = tutorTrackServiceDao.getStudentIdFromMobile(request.getMobileOrEmail());
			}else{
				studentId = tutorTrackServiceDao.getStudentIdFromEmail(request.getMobileOrEmail());
			}
			if (studentId != null) {
				StudentInfo studentInfo = tutorTrackServiceDao.getStudentInfo(studentId);
				if (studentInfo.getPassword().equalsIgnoreCase(request.getPassword())) {
					response.setStatus("SUCCESS");
					response.setMessage("Successfully Signed In");
					response.setStudentInfo(studentInfo);
				} else {
					response.setStatus("FAIL");
					response.setMessage("User/Password didn't match");
				}

			} else {
				response.setStatus("FAIL");
				response.setMessage("You are not registered yet!");
			}
		} catch (Exception e) {
			throw new TutorTrackException(ErrorCode.ERROR_SELECTING_STUDENT_INFO);
		}
		return response;
	}
	
	@Override
	public StudentInfoResponse updateStudentInfo(StudentInfo studentInfo) {
		StudentInfoResponse response = new StudentInfoResponse();
		String studentId = null;
		try{
			tutorTrackServiceDao.updateStudentInfo(studentInfo);
			studentId = tutorTrackServiceDao.getStudentIdFromMobile(studentInfo.getMobile());
			studentInfo = tutorTrackServiceDao.getStudentInfo(studentId);
			response.setMessage("Successfully updated.");
			response.setStudentInfo(studentInfo);
			response.setStatus("SUCCESS");
		}catch (Exception ex) {
			throw new TutorTrackException(ErrorCode.ERROR_UPDATING_STUDENT_INFO);
		}
		return response;
	}

}
