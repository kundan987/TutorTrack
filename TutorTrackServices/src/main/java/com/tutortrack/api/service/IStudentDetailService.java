package com.tutortrack.api.service;

import com.tutortrack.api.dto.StudentInfo;
import com.tutortrack.api.request.StudentInfoRequest;
import com.tutortrack.api.response.StudentInfoResponse;

public interface IStudentDetailService {

	StudentInfoResponse getStudentInfo(StudentInfoRequest request);

	StudentInfoResponse updateStudentInfo(StudentInfo studentInfo);

}
