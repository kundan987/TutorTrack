package com.tutortrack.api.service;

import com.tutortrack.api.dto.TutorInfo;
import com.tutortrack.api.request.TutorInfoRequest;
import com.tutortrack.api.request.TutorSearchRequest;
import com.tutortrack.api.response.TutorInfoResponse;
import com.tutortrack.api.response.TutorSearchResponse;

public interface ITutorDetailService {
	
	TutorSearchResponse getTutorsDetail(TutorSearchRequest request);

	TutorInfoResponse getTutorInfo(TutorInfoRequest request);

	TutorInfoResponse updateTutorInfo(TutorInfo tutorInfo);

}
