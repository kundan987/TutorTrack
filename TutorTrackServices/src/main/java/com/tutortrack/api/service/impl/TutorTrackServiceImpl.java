package com.tutortrack.api.service.impl;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import com.tutortrack.api.dao.ITutorTrackServiceDao;
import com.tutortrack.api.datastructure.TutorAggregateRating;
import com.tutortrack.api.datastructure.TutorClassSubject;
import com.tutortrack.api.datastructure.TutorReviewRating;
import com.tutortrack.api.dto.StudentInfo;
import com.tutortrack.api.dto.TutorInfo;
import com.tutortrack.api.exception.ErrorCode;
import com.tutortrack.api.exception.TutorTrackException;
import com.tutortrack.api.request.UserInfoRequest;
import com.tutortrack.api.request.ValidateNumberRequest;
import com.tutortrack.api.response.StudentBasicResponse;
import com.tutortrack.api.response.TutorBasicResponse;
import com.tutortrack.api.response.TutorTrackResponse;
import com.tutortrack.api.response.UserInfoResponse;
import com.tutortrack.api.response.ValidateNumberResponse;
import com.tutortrack.api.service.ITutorTrackService;

/*
 * Author : Kundan Mishra
 * Date   : 08-June-2016
 */
@Service("tutorTrackService")
public class TutorTrackServiceImpl implements ITutorTrackService {
	
	@Autowired
	ITutorTrackServiceDao tutorTrackServiceDao;

	private static final Logger LOG = LoggerFactory.getLogger(TutorTrackServiceImpl.class);

	@Override
	public void sendOtp(String mobileNumber) {
		// TODO Auto-generated method stub

	}

	@Override
	public ValidateNumberResponse validateNumber(ValidateNumberRequest request) {

		LOG.info(request.toString());

		ValidateNumberResponse response = new ValidateNumberResponse();
		response.setValidated(true);

		return response;
	}

	@Override
	public TutorBasicResponse createTutorInfo(TutorInfo tutorInfo) {
		TutorBasicResponse response = new TutorBasicResponse();
		TutorAggregateRating tutorAggregateRating = new TutorAggregateRating();
		TutorTrackResponse ttresponse = new TutorTrackResponse();
		try{
			String tutorId = generateTutorId();
			tutorInfo.setTutorId(tutorId);
			tutorTrackServiceDao.insertTutorInfo(tutorInfo);
			tutorAggregateRating.setAvgRating(0);
			tutorAggregateRating.setNumReviews(0);
			tutorAggregateRating.setTutorId(tutorInfo.getTutorId());
			ttresponse = createTutorAggregateRating(tutorAggregateRating);
			response.setEmail(tutorInfo.getEmail());
			response.setMobile(tutorInfo.getMobile());
			response.setName(tutorInfo.getName());
			response.setTutorId(tutorId);
			response.setOtpCode(generateOtp());
			if(!ttresponse.getStatus().equalsIgnoreCase("SUCCESS")){
				response.setStatus("FAIL");
			}
		}catch (DuplicateKeyException ex) {
			throw new TutorTrackException(ErrorCode.DUPLICATE_KEY_EXCEPTION,"Mobile/Email is already registered.");
		}catch (Exception ex) {
			throw new TutorTrackException(ErrorCode.ERROR_INSERTING_TUTOR_INFO);
		}
		return response;
	}
	
	private String generateTutorId() {
		String tutorId = null;
		try {
			SecureRandom prng = SecureRandom.getInstance("SHA1PRNG");
			Integer randNumber = new Integer(prng.nextInt());
			if (randNumber < 0) {
				randNumber = randNumber * (-1);
			}
			tutorId = "T"+randNumber.toString();

		} catch (NoSuchAlgorithmException ex) {
			LOG.error(ex.toString());
		}

		return tutorId;
	}
	
	private Integer generateOtp() {
		Integer otpCode = null;
		Random random = new Random();
		otpCode = random.nextInt(9999 - 1000) + 999;

		return otpCode;
	}

	@Override
	public StudentBasicResponse createStudentInfo(StudentInfo studentInfo) {
		StudentBasicResponse response = new StudentBasicResponse();
		try{
			String studentId = generateStudentId();
			studentInfo.setStudentId(studentId);
			tutorTrackServiceDao.insertStudentInfo(studentInfo);
			response.setEmail(studentInfo.getEmail());
			response.setMobile(studentInfo.getMobile());
			response.setName(studentInfo.getName());
			response.setStudentId(studentId);
			response.setOtpCode(generateOtp());
			response.setStatus("SUCCESS");
		}catch (DuplicateKeyException ex) {
			throw new TutorTrackException(ErrorCode.DUPLICATE_KEY_EXCEPTION,"Mobile/Email is already registered.");
		}catch (Exception ex) {
			throw new TutorTrackException(ErrorCode.ERROR_INSERTING_STUDENT_INFO);
		}
		return response;
	}
	
	private String generateStudentId() {
		String studentId = null;
		try {
			SecureRandom prng = SecureRandom.getInstance("SHA1PRNG");
			Integer randNumber = new Integer(prng.nextInt());
			if (randNumber < 0) {
				randNumber = randNumber * (-1);
			}
			studentId = "S"+randNumber.toString();

		} catch (NoSuchAlgorithmException ex) {
			LOG.error(ex.toString());
		}

		return studentId;
	}
	
	@Override
	public TutorTrackResponse createTutorClassSubjectMapping(TutorClassSubject tutorClassSubject) {
		TutorTrackResponse response = new TutorTrackResponse();
		try{
			tutorTrackServiceDao.insertTutorClassSubjectMapping(tutorClassSubject);
			response.setStatus("SUCCESS");
		}catch (DuplicateKeyException ex) {
			throw new TutorTrackException(ErrorCode.DUPLICATE_KEY_EXCEPTION,"Class Subject mapping already exists");
		}catch (Exception ex) {
			throw new TutorTrackException(ErrorCode.ERROR_INSERTING_TUTOR_CLASS_SUBJECT_MAPPING);
		}
		return response;
	}
	
	@Override
	public TutorTrackResponse createTutorReviewRatingMapping(TutorReviewRating tutorReviewRating) {
		TutorTrackResponse response = new TutorTrackResponse();
		Integer reviewPresent = 0;
		if(tutorReviewRating.getReview() != null){
			 reviewPresent = 1;
		}
		try{
			Integer count = tutorTrackServiceDao.getTutorRatingCount(tutorReviewRating.getTutorId());
			tutorTrackServiceDao.insertTutorReviewRatingMapping(tutorReviewRating);
			tutorTrackServiceDao.updateTutorAggregateRating(reviewPresent, count, tutorReviewRating.getRating(), tutorReviewRating.getTutorId());
			response.setStatus("SUCCESS");
		}catch (DuplicateKeyException ex) {
			throw new TutorTrackException(ErrorCode.DUPLICATE_KEY_EXCEPTION,"Student Id review rating mapping already exists");
		}catch (Exception ex) {
			throw new TutorTrackException(ErrorCode.ERROR_INSERTING_TUTOR_REVIEW_RATING_MAPPING);
		}
		return response;
	}
	
	@Override
	public TutorTrackResponse createTutorAggregateRating(TutorAggregateRating tutorAggregateRating) {
		TutorTrackResponse response = new TutorTrackResponse();
		try{
			tutorTrackServiceDao.insertTutorAggregateRating(tutorAggregateRating);
			response.setStatus("SUCCESS");
		}catch (DuplicateKeyException ex) {
			throw new TutorTrackException(ErrorCode.DUPLICATE_KEY_EXCEPTION,"Tutor Aggregate Rating already exists");
		}catch (Exception ex) {
			throw new TutorTrackException(ErrorCode.ERROR_INSERTING_TUTOR_AGGREGATE_RATING);
		}
		return response;
	}
	
	@Override
	public UserInfoResponse getUserInfo(UserInfoRequest request) {

		UserInfoResponse response = new UserInfoResponse();
		String studentId = null;
		String tutorId = null;
		try {
			LOG.info(request.getMobileOrEmail());
			if (NumberUtils.isNumber(request.getMobileOrEmail())) {
				studentId = tutorTrackServiceDao.getStudentIdFromMobile(request.getMobileOrEmail());
				tutorId = tutorTrackServiceDao.getTutorIdFromMobile(request.getMobileOrEmail());
			} else {
				studentId = tutorTrackServiceDao.getStudentIdFromEmail(request.getMobileOrEmail());
				tutorId = tutorTrackServiceDao.getTutorIdFromEmail(request.getMobileOrEmail());
			}
			if (studentId != null) {
				StudentInfo studentInfo = tutorTrackServiceDao.getStudentInfo(studentId);
				if (studentInfo.getPassword().equalsIgnoreCase(request.getPassword())) {
					response.setStatus("SUCCESS");
					response.setMessage("Successfully Signed In");
					response.setStudentInfo(studentInfo);
				} else {
					response.setStatus("FAIL");
					response.setMessage("User/Password didn't match");
				}

			} else if (tutorId != null) {
				TutorInfo tutorInfo = tutorTrackServiceDao.getTutorInfo(tutorId);
				if (tutorInfo.getPassword().equalsIgnoreCase(request.getPassword())) {
					response.setStatus("SUCCESS");
					response.setMessage("Successfully Signed In");
					response.setTutorInfo(tutorInfo);
				} else {
					response.setStatus("FAIL");
					response.setMessage("User/Password didn't match");
				}

			}else {
				response.setStatus("FAIL");
				response.setMessage("You are not registered yet!.");
			}

		} catch (Exception e) {
			throw new TutorTrackException(ErrorCode.ERROR_SELECTING_USER_INFO);
		}
		return response;
	}
}
