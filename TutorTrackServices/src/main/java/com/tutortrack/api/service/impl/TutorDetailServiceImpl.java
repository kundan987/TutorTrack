package com.tutortrack.api.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tutortrack.api.constants.SortBy;
import com.tutortrack.api.constants.TutorDetailFetchType;
import com.tutortrack.api.dao.ITutorTrackServiceDao;
import com.tutortrack.api.datastructure.TutorAggregateRating;
import com.tutortrack.api.datastructure.TutorClassSubject;
import com.tutortrack.api.datastructure.TutorDetail;
import com.tutortrack.api.datastructure.TutorReviewRating;
import com.tutortrack.api.dto.TutorInfo;
import com.tutortrack.api.exception.ErrorCode;
import com.tutortrack.api.exception.TutorTrackException;
import com.tutortrack.api.request.TutorInfoRequest;
import com.tutortrack.api.request.TutorSearchRequest;
import com.tutortrack.api.response.TutorInfoResponse;
import com.tutortrack.api.response.TutorSearchResponse;
import com.tutortrack.api.service.ITutorDetailService;

@Service("tutorDetailService")
public class TutorDetailServiceImpl implements ITutorDetailService {

	@Autowired
	ITutorTrackServiceDao tutorTrackServiceDao;

	private static final Logger LOG = LoggerFactory.getLogger(TutorDetailServiceImpl.class);

	@Override
	public TutorInfoResponse getTutorInfo(TutorInfoRequest request) {

		TutorInfoResponse response = new TutorInfoResponse();
		String tutorId = null;
		try {
			LOG.info(request.getMobileOrEmail());
			if (NumberUtils.isNumber(request.getMobileOrEmail())) {
				tutorId = tutorTrackServiceDao.getTutorIdFromMobile(request.getMobileOrEmail());
			} else {
				tutorId = tutorTrackServiceDao.getTutorIdFromEmail(request.getMobileOrEmail());
			}
			if (tutorId != null) {
				TutorInfo tutorInfo = tutorTrackServiceDao.getTutorInfo(tutorId);
				if (tutorInfo.getPassword().equalsIgnoreCase(request.getPassword())) {
					response.setStatus("SUCCESS");
					response.setMessage("Successfully Signed In");
					response.setTutorInfo(tutorInfo);
				} else {
					response.setStatus("FAIL");
					response.setMessage("User/Password didn't match");
				}

			} else {
				response.setStatus("FAIL");
				response.setMessage("You are not registered yet!");
			}
		} catch (Exception e) {
			throw new TutorTrackException(ErrorCode.ERROR_SELECTING_TUTOR_INFO);
		}
		return response;
	}

	@Override
	public TutorSearchResponse getTutorsDetail(TutorSearchRequest request) {

		LOG.info(request.getFetchType().toString());
		TutorSearchResponse response = new TutorSearchResponse();
		try {
			if (TutorDetailFetchType.FETCH_WITH_BASIC_DETAILS.equals(request.getFetchType())) {
				response = prepareTutorsBasicDetail(request);
			} else if (TutorDetailFetchType.FETCH_WITH_ALL_DETAILS.equals(request.getFetchType())) {
				response = prepareTutorsFullDetail(request);
			}
		} catch (Exception e) {
			throw new TutorTrackException(ErrorCode.ERROR_SELECTING_TUTORS_DETAIL);
		}
		return response;
	}

	private TutorSearchResponse prepareTutorsBasicDetail(TutorSearchRequest request) {

		TutorSearchResponse response = new TutorSearchResponse();
		List<TutorDetail> tutorsDetail = new ArrayList<TutorDetail>();
		List<TutorInfo> tutorsInfo = new ArrayList<TutorInfo>();
		tutorsInfo = tutorTrackServiceDao.getTutorsBasicInfo();
		TutorDetail tutorDetail;
		for (TutorInfo tutorInfo : tutorsInfo) {
			tutorDetail = new TutorDetail();

			TutorAggregateRating tutorAggregateRating = new TutorAggregateRating();
			tutorAggregateRating = tutorTrackServiceDao.getTutorAggregateRating(tutorInfo.getTutorId());

			List<TutorClassSubject> tutorClassSubjects = new ArrayList<TutorClassSubject>();
			tutorClassSubjects = tutorTrackServiceDao.getTutorSubjects(tutorInfo.getTutorId());

			tutorDetail = prepareTutorBasicDetails(tutorInfo, tutorAggregateRating, tutorClassSubjects);

			tutorsDetail.add(tutorDetail);

		}
		if (SortBy.EXP.toString().equalsIgnoreCase(request.getSortBy().toString())) {
			Collections.sort(tutorsDetail, new Comparator<TutorDetail>() {
				@Override
				public int compare(TutorDetail tutorDetail1, TutorDetail tutorDetail2) {

					return tutorDetail2.experience.compareTo(tutorDetail1.experience);
				}
			});

		}
		if (SortBy.RATING.toString().equalsIgnoreCase(request.getSortBy().toString())) {
			Collections.sort(tutorsDetail, new Comparator<TutorDetail>() {
				@Override
				public int compare(TutorDetail tutorDetail1, TutorDetail tutorDetail2) {
					Integer rating1 = (int)tutorDetail1.rating;
					Integer rating2 = (int)tutorDetail2.rating;

					return rating2.compareTo(rating1);
				}
			});

		}
		if (SortBy.FEE_INC.toString().equalsIgnoreCase(request.getSortBy().toString())) {
			Collections.sort(tutorsDetail, new Comparator<TutorDetail>() {
				@Override
				public int compare(TutorDetail tutorDetail1, TutorDetail tutorDetail2) {
					return tutorDetail1.fees.compareTo(tutorDetail2.fees);
				}
			});

		}
		if (SortBy.FEE_DEC.toString().equalsIgnoreCase(request.getSortBy().toString())) {
			Collections.sort(tutorsDetail, new Comparator<TutorDetail>() {
				@Override
				public int compare(TutorDetail tutorDetail1, TutorDetail tutorDetail2) {
					return tutorDetail2.fees.compareTo(tutorDetail1.fees);
				}
			});

		}
		response.setTutors(tutorsDetail);
		return response;
	}

	private TutorDetail prepareTutorBasicDetails(TutorInfo tutorInfo, TutorAggregateRating tutorAggregateRating,
			List<TutorClassSubject> tutorClassSubjects) {

		TutorDetail tutorDetail = new TutorDetail();
		tutorDetail.setTutorId(tutorInfo.getTutorId());
		tutorDetail.setName(tutorInfo.getName());
		tutorDetail.setImageUrl(tutorInfo.getImageUrl());
		tutorDetail.setExperience(tutorInfo.getExperience());
		tutorDetail.setDistance(null);
		tutorDetail.setFees(0);
		if (tutorAggregateRating != null) {
			tutorDetail.setRating(tutorAggregateRating.getAvgRating());
			tutorDetail.setNumreviews(tutorAggregateRating.getNumReviews());
		}
		if (!(tutorClassSubjects == null || tutorClassSubjects.isEmpty())) {
			List<String> teaches = new ArrayList<String>();
			Integer fees = 0;

			for (TutorClassSubject tutorClassSubject : tutorClassSubjects) {
				teaches.add(tutorClassSubject.getSubject());
				if (tutorClassSubject.getFee() > fees)
					fees = tutorClassSubject.getFee();
			}
			tutorDetail.setTeaches(teaches);
			tutorDetail.setFees(fees);
		}
		return tutorDetail;
	}

	private TutorSearchResponse prepareTutorsFullDetail(TutorSearchRequest request) {
		TutorSearchResponse response = new TutorSearchResponse();
		List<TutorDetail> tutorsDetail = new ArrayList<TutorDetail>();

		List<TutorInfo> tutorsInfo = new ArrayList<TutorInfo>();
		tutorsInfo = tutorTrackServiceDao.getTutorsFullInfo();

		for (TutorInfo tutorInfo : tutorsInfo) {
			TutorDetail tutorDetail = new TutorDetail();

			List<TutorReviewRating> tutorReviewRating = new ArrayList<TutorReviewRating>();
			tutorReviewRating = tutorTrackServiceDao.getTutorRatingReviews(tutorInfo.getTutorId());

			TutorAggregateRating tutorAggregateRating = new TutorAggregateRating();
			tutorAggregateRating = tutorTrackServiceDao.getTutorAggregateRating(tutorInfo.getTutorId());

			List<TutorClassSubject> tutorClassSubjects = new ArrayList<TutorClassSubject>();
			tutorClassSubjects = tutorTrackServiceDao.getTutorSubjects(tutorInfo.getTutorId());

			tutorDetail = prepareTutorFullDetails(tutorInfo, tutorAggregateRating, tutorClassSubjects,
					tutorReviewRating);

			tutorsDetail.add(tutorDetail);

		}
		response.setTutors(tutorsDetail);
		return response;
	}

	private TutorDetail prepareTutorFullDetails(TutorInfo tutorInfo, TutorAggregateRating tutorAggregateRating,
			List<TutorClassSubject> tutorClassSubjects, List<TutorReviewRating> tutorReviewRatings) {

		TutorDetail tutorDetail = new TutorDetail();
		tutorDetail.setTutorId(tutorInfo.getTutorId());
		tutorDetail.setName(tutorInfo.getName());
		tutorDetail.setEmail(tutorInfo.getEmail());
		tutorDetail.setMobile(tutorInfo.getMobile());
		tutorDetail.setGender(tutorInfo.getGender());
		tutorDetail.setDob(tutorInfo.getDob());
		tutorDetail.setAddress(tutorInfo.getAddress());
		tutorDetail.setCity(tutorInfo.getCity());
		tutorDetail.setState(tutorInfo.getState());
		tutorDetail.setPincode(tutorInfo.getPincode());
		tutorDetail.setLocation(tutorInfo.getLocation());
		tutorDetail.setImageUrl(tutorInfo.getImageUrl());
		tutorDetail.setDescription(tutorInfo.getDescription());
		tutorDetail.setQualification(tutorInfo.getQualification());
		tutorDetail.setExperience(tutorInfo.getExperience());
		tutorDetail.setDistance(null);
		if (tutorAggregateRating != null) {
			tutorDetail.setRating(tutorAggregateRating.getAvgRating());
			tutorDetail.setNumreviews(tutorAggregateRating.getNumReviews());
		}
		if (!(tutorClassSubjects == null || tutorClassSubjects.isEmpty())) {
			tutorDetail.setTutorClassSubject(tutorClassSubjects);
		}
		if (!(tutorReviewRatings == null || tutorReviewRatings.isEmpty())) {
			tutorDetail.setTutorReviewRating(tutorReviewRatings);
		}
		return tutorDetail;
	}
	
	@Override
	public TutorInfoResponse updateTutorInfo(TutorInfo tutorInfo) {
		TutorInfoResponse response = new TutorInfoResponse();
		String tutorId = null;
		try{
			tutorTrackServiceDao.updateTutorInfo(tutorInfo);
			tutorId = tutorTrackServiceDao.getTutorIdFromMobile(tutorInfo.getMobile());
			tutorInfo = tutorTrackServiceDao.getTutorInfo(tutorId);
			response.setMessage("Successfully updated.");
			response.setTutorInfo(tutorInfo);
			response.setStatus("SUCCESS");
		}catch (Exception ex) {
			throw new TutorTrackException(ErrorCode.ERROR_UPDATING_TUTOR_INFO);
		}
		return response;
	}

}
