package com.tutortrack.api.service;

import com.tutortrack.api.datastructure.TutorAggregateRating;
import com.tutortrack.api.datastructure.TutorClassSubject;
import com.tutortrack.api.datastructure.TutorReviewRating;
import com.tutortrack.api.dto.StudentInfo;
import com.tutortrack.api.dto.TutorInfo;
import com.tutortrack.api.request.UserInfoRequest;
import com.tutortrack.api.request.ValidateNumberRequest;
import com.tutortrack.api.response.StudentBasicResponse;
import com.tutortrack.api.response.TutorBasicResponse;
import com.tutortrack.api.response.TutorTrackResponse;
import com.tutortrack.api.response.UserInfoResponse;
import com.tutortrack.api.response.ValidateNumberResponse;

/*
 * Author : Kundan Mishra
 * Date   : 08-June-2016
 */
public interface ITutorTrackService {
	
	void sendOtp(String mobileNumber);
	ValidateNumberResponse validateNumber(ValidateNumberRequest request);
	TutorBasicResponse createTutorInfo(TutorInfo tutorInfo);
	StudentBasicResponse createStudentInfo(StudentInfo studentInfo);
	TutorTrackResponse createTutorClassSubjectMapping(TutorClassSubject tutorClassSubject);
	TutorTrackResponse createTutorReviewRatingMapping(TutorReviewRating tutorReviewRating);
	TutorTrackResponse createTutorAggregateRating(TutorAggregateRating tutorAggregateRating);
	UserInfoResponse getUserInfo(UserInfoRequest request);

}
