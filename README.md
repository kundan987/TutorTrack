This project is related to TutorTrack Android Application.

This project provides back-end support to android application by exposing diffrent API's.

To execute this project import it in Eclipse/InteliiJ as a maven project.

Build project using command after going to project directory in terminal.

Command : mvn clean install (It will build the project with all the dependency)

To Run the project: Run on server TutorTrackWeb project (support Apache Tomcat 8)

It will open localhost:8080/TutorTrackWeb/swagger-ui.html

Swagger is integrated to test apis.