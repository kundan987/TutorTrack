DROP DATABASE IF EXISTS tutortrack;
CREATE DATABASE tutortrack;
USE tutortrack;

DROP TABLE IF EXISTS tutor_info;
DROP TABLE IF EXISTS student_info;
DROP TABLE IF EXISTS tutor_review_rating_mapping;
DROP TABLE IF EXISTS tutor_class_subject_mapping;
DROP TABLE IF EXISTS tutor_aggregate_rating;

CREATE TABLE tutor_info (
  tutor_id varchar(20) NOT NULL UNIQUE,
  name varchar(50),
  email varchar(50) NOT NULL UNIQUE,
  mobile varchar(15) NOT NULL UNIQUE,
  gender varchar(10),
  dob varchar(20),
  password varchar(36),
  address varchar(200), 
  city varchar(50), 
  state varchar(50),
  pincode varchar(10),
  location varchar(30),
  image_url varchar(200),
  qualification varchar(100),
  experience varchar(20), 
  description varchar(250),
  created DATETIME NOT NULL,
  updated DATETIME NOT NULL,
  CONSTRAINT pk_tutor_info_tutor_id PRIMARY KEY (tutor_id)
);

CREATE TABLE student_info (
  student_id varchar(20) NOT NULL UNIQUE,
  name varchar(50),
  email varchar(50) NOT NULL UNIQUE,
  mobile varchar(15) NOT NULL UNIQUE,
  gender varchar(10),
  dob varchar(20),
  password varchar(36),
  address varchar(200), 
  city varchar(50), 
  state varchar(50),
  pincode varchar(10),
  location varchar(30),
  image_url varchar(200),
  board varchar(10),
  class varchar(10),
  description varchar(250),
  created DATETIME NOT NULL,
  updated DATETIME NOT NULL,
  CONSTRAINT pk_student_info_student_id PRIMARY KEY (student_id)
);

CREATE TABLE tutor_class_subject_mapping  (
  id int(11) NOT NULL AUTO_INCREMENT,
  class varchar(20) NOT NULL,
  subject varchar(50) NOT NULL,
  fee varchar(10) NOT NULL,
  tutor_id varchar(20) NOT NULL,
  created DATETIME NOT NULL,
  updated DATETIME NOT NULL,
  CONSTRAINT pk_tutor_class_subject_mapping_id PRIMARY KEY (id),
  CONSTRAINT fk_tutor_class_subject_mapping_tutor_id FOREIGN KEY (tutor_id) REFERENCES tutor_info(tutor_id),
  CONSTRAINT fk_tutor_class_subject_mapping_class_subject UNIQUE (class,subject)
);

CREATE TABLE tutor_review_rating_mapping (
  id int(11) NOT NULL AUTO_INCREMENT,
  rating varchar(10) NOT NULL,
  review varchar(250),
  tutor_id varchar(20) NOT NULL,
  student_id varchar(20) NOT NULL,
  created DATETIME NOT NULL,
  updated DATETIME NOT NULL,
  CONSTRAINT pk_tutor_review_rating_mapping_id PRIMARY KEY (id),
  CONSTRAINT fk_tutor_review_rating_mapping_tutor_id FOREIGN KEY (tutor_id) REFERENCES tutor_info(tutor_id),
  CONSTRAINT fk_tutor_review_rating_mapping_student_id FOREIGN KEY (student_id) REFERENCES student_info(student_id),
  CONSTRAINT fk_tutor_review_rating_mapping_rating_review_student_id UNIQUE (rating,review,student_id)
);

CREATE TABLE tutor_aggregate_rating (
  id int(11) NOT NULL AUTO_INCREMENT,
  avg_rating varchar(10) NOT NULL,
  num_reviews varchar(10),
  tutor_id varchar(20) NOT NULL,
  created DATETIME NOT NULL,
  updated DATETIME NOT NULL,
  CONSTRAINT pk_tutor_aggregate_rating_id PRIMARY KEY (id),
  CONSTRAINT fk_tutor_aggregate_rating_tutor_id FOREIGN KEY (tutor_id) REFERENCES tutor_info(tutor_id),
  CONSTRAINT fk_tutor_aggregate_rating_avg_rating_num_reviews UNIQUE (avg_rating,num_reviews,tutor_id)
);
