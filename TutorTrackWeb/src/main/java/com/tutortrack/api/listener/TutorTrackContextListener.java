package com.tutortrack.api.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.tutortrack.api.service.ITutorTrackService;

public class TutorTrackContextListener implements ServletContextListener {
	@SuppressWarnings("unused")
	@Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        WebApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContextEvent.getServletContext());
        ITutorTrackService iTutorTrackService = context.getBean(ITutorTrackService.class);
             
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
    	// do nothing
    }

}
