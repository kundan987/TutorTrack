package com.tutortrack.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tutortrack.api.dto.TutorInfo;
import com.tutortrack.api.exception.ErrorCode;
import com.tutortrack.api.exception.TutorTrackException;
import com.tutortrack.api.request.TutorInfoRequest;
import com.tutortrack.api.request.TutorSearchRequest;
import com.tutortrack.api.response.TutorInfoResponse;
import com.tutortrack.api.response.TutorSearchResponse;
import com.tutortrack.api.service.ITutorDetailService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Author : kundan
 * Date   : 14-Jun-2016
 */
@Api(value="Tutor Detail Service")
@RestController
@RequestMapping("api/tt/tutor")
public class TutorDetailController {
	
	@Autowired
	private ITutorDetailService tutorDetailService;
	
	@RequestMapping(value = "getTutorsDetail", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="get Tutors detail",response=TutorSearchResponse.class)
	public TutorSearchResponse getTutorsDetail(@RequestBody(required=true)TutorSearchRequest request){
		TutorSearchResponse response = new TutorSearchResponse();
		try {
			response = tutorDetailService.getTutorsDetail(request);
			
	    	response.setStatus("SUCCESS");
		} catch (Exception e) {
			response.setStatus("FAIL");
			response.setMessage(e.getMessage());
		}
    	return response;        
    }
	
	@RequestMapping(value = "getTutorInfo", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="get Tutor info",response=TutorInfoResponse.class)
	public TutorInfoResponse getTutorInfo(@RequestBody(required=true)TutorInfoRequest request){
		TutorInfoResponse response = new TutorInfoResponse();
		try {
			response = tutorDetailService.getTutorInfo(request);
		} catch (TutorTrackException te){
			response.setStatus("FAIL");
			response.setErrorCode(te.getErrorCode().toString());
			response.setMessage("User is not registered.");
		}catch (Exception e) {
			response.setStatus("FAIL");
			response.setMessage(e.getMessage());
			response.setErrorCode(ErrorCode.INTERNAL_SERVER_ERROR.toString());
		}
    	return response;        
    }
	
	@RequestMapping(value = "updateTutorInfo", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="update Tutor info",response=TutorInfoResponse.class)
	public TutorInfoResponse updateTutorInfo(@RequestBody(required=true)TutorInfo request){
		TutorInfoResponse response = new TutorInfoResponse();
		try {
			response = tutorDetailService.updateTutorInfo(request);
		} catch (TutorTrackException te){
			response.setStatus("FAIL");
			response.setErrorCode(te.getErrorCode().toString());
			response.setMessage(te.getMessage());
		}catch (Exception e) {
			response.setStatus("FAIL");
			response.setMessage(e.getMessage());
			response.setErrorCode(ErrorCode.INTERNAL_SERVER_ERROR.toString());
		}
    	return response;        
    }

}
