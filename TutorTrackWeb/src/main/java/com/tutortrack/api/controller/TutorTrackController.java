package com.tutortrack.api.controller;

import com.tutortrack.api.datastructure.TutorClassSubject;
import com.tutortrack.api.datastructure.TutorReviewRating;
import com.tutortrack.api.dto.StudentInfo;
import com.tutortrack.api.dto.TutorInfo;
import com.tutortrack.api.exception.ErrorCode;
import com.tutortrack.api.exception.TutorTrackException;
import com.tutortrack.api.request.UserInfoRequest;
import com.tutortrack.api.request.ValidateNumberRequest;
import com.tutortrack.api.response.StudentBasicResponse;
import com.tutortrack.api.response.TutorBasicResponse;
import com.tutortrack.api.response.TutorTrackResponse;
import com.tutortrack.api.response.UserInfoResponse;
import com.tutortrack.api.response.ValidateNumberResponse;
import com.tutortrack.api.service.ITutorTrackService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@Api(value="TutorTrack Service")
@RestController
@RequestMapping("api/tt")
public class TutorTrackController {
	
	@Autowired
	private ITutorTrackService tutorTrackService;

	@RequestMapping(value = "validateNumber", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="Validate opt of user",response=ValidateNumberResponse.class)
	public ValidateNumberResponse validateNumber(@RequestBody(required=true)ValidateNumberRequest request){
		ValidateNumberResponse response = new ValidateNumberResponse();
		try {
			response = tutorTrackService.validateNumber(request);
			
	    	response.setStatus("SUCCESS");
		} catch (Exception e) {
			response.setStatus("FAIL");
			response.setMessage(e.getMessage());
		}
    	return response;        
    }
	
	@RequestMapping(value = "createTutorInfo", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="Register user as a tutor",response=TutorBasicResponse.class)
	public TutorBasicResponse createTutorInfo(@RequestBody(required=true)TutorInfo tutorInfo){
		TutorBasicResponse response = new TutorBasicResponse();
		try {
			response = tutorTrackService.createTutorInfo(tutorInfo);
			
	    	response.setStatus("SUCCESS");
		} catch (Exception e) {
			response.setStatus("FAIL");
			response.setMessage(e.getMessage());
		}
    	return response;        
    }
	
	@RequestMapping(value = "createStudentInfo", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="Register user as a student",response=StudentBasicResponse.class)
	public StudentBasicResponse createStudentInfo(@RequestBody(required=true)StudentInfo studentInfo){
		StudentBasicResponse response = new StudentBasicResponse();
		try {
			response = tutorTrackService.createStudentInfo(studentInfo);
			
	    	response.setStatus("SUCCESS");
		} catch (Exception e) {
			response.setStatus("FAIL");
			response.setMessage(e.getMessage());
		}
    	return response;        
    }
	
	@RequestMapping(value = "createTutorClassSubjectMapping", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="Create tutor's class subjects mapping",response=TutorTrackResponse.class)
	public TutorTrackResponse createTutorClassSubjectMapping(@RequestBody(required=true)TutorClassSubject tutorClassSubject){
		TutorTrackResponse response = new TutorTrackResponse();
		try {
			response = tutorTrackService.createTutorClassSubjectMapping(tutorClassSubject);
			
	    	response.setStatus("SUCCESS");
		} catch (Exception e) {
			response.setStatus("FAIL");
			response.setMessage(e.getMessage());
		}
    	return response;        
    }
	
	@RequestMapping(value = "createTutorReviewRatingMapping", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="Create tutor's review rating mapping",response=TutorTrackResponse.class)
	public TutorTrackResponse createTutorReviewRatingMapping(@RequestBody(required=true)TutorReviewRating tutorReviewRating){
		TutorTrackResponse response = new TutorTrackResponse();
		try {
			response = tutorTrackService.createTutorReviewRatingMapping(tutorReviewRating);
			
	    	response.setStatus("SUCCESS");
		} catch (Exception e) {
			response.setStatus("FAIL");
			response.setMessage(e.getMessage());
		}
    	return response;        
    }
	
	@RequestMapping(value = "getUserInfo", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="get User info",response=UserInfoResponse.class)
	public UserInfoResponse getUserInfo(@RequestBody(required=true)UserInfoRequest request){
		UserInfoResponse response = new UserInfoResponse();
		try {
			response = tutorTrackService.getUserInfo(request);
		} catch (TutorTrackException te){
			response.setStatus("FAIL");
			response.setErrorCode(te.getErrorCode().toString());
			response.setMessage("User is not registered.");
		}catch (Exception e) {
			response.setStatus("FAIL");
			response.setMessage(e.getMessage());
			response.setErrorCode(ErrorCode.INTERNAL_SERVER_ERROR.toString());
		}
    	return response;        
    }
}
