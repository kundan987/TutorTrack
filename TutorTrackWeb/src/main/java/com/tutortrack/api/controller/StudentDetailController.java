/**
 * 
 */
package com.tutortrack.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tutortrack.api.dto.StudentInfo;
import com.tutortrack.api.exception.ErrorCode;
import com.tutortrack.api.exception.TutorTrackException;
import com.tutortrack.api.request.StudentInfoRequest;
import com.tutortrack.api.response.StudentInfoResponse;
import com.tutortrack.api.service.IStudentDetailService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Author : kundan
 * Date   : 18-Jun-2016
 */
@Api(value="Student Detail Service")
@RestController
@RequestMapping("api/tt/student")
public class StudentDetailController {
	
	@Autowired
	private IStudentDetailService studentDetailService;
	
	@RequestMapping(value = "getStudentInfo", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="get Student info",response=StudentInfoResponse.class)
	public StudentInfoResponse getStudentInfo(@RequestBody(required=true)StudentInfoRequest request){
		StudentInfoResponse response = new StudentInfoResponse();
		try {
			response = studentDetailService.getStudentInfo(request);
		} catch (TutorTrackException te){
			response.setStatus("FAIL");
			response.setErrorCode(te.getErrorCode().toString());
			response.setMessage("User is not registered.");
		}catch (Exception e) {
			response.setStatus("FAIL");
			response.setMessage(e.getMessage());
			response.setErrorCode(ErrorCode.INTERNAL_SERVER_ERROR.toString());
		}
    	return response;        
    }
	
	@RequestMapping(value = "updateStudentInfo", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="update Student info",response=StudentInfoResponse.class)
	public StudentInfoResponse updateStudentInfo(@RequestBody(required=true)StudentInfo request){
		StudentInfoResponse response = new StudentInfoResponse();
		try {
			response = studentDetailService.updateStudentInfo(request);
		} catch (TutorTrackException te){
			response.setStatus("FAIL");
			response.setErrorCode(te.getErrorCode().toString());
			response.setMessage(te.getMessage());
		}catch (Exception e) {
			response.setStatus("FAIL");
			response.setMessage(e.getMessage());
			response.setErrorCode(ErrorCode.INTERNAL_SERVER_ERROR.toString());
		}
    	return response;        
    }

}
